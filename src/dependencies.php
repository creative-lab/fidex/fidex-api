<?php

const URL_API = "http://fidexapp.com.br/img/avatar/";
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['destino_avatar'] = __DIR__ . '/../img/avatar/';

// Service factory for the ORM
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule){
 
   return $capsule;
 
};

$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

    

/*
//Firebase
$container['firebase'] = function($c){

    $serviceAccount = \Kreait\Firebase\ServiceAccount::fromJsonFile(__DIR__.'/../firebase_credentials.json');

    $firebase = (new \Kreait\Firebase\Factory)->withServiceAccount($serviceAccount)->create();

    return $firebase->getAuth();

};

*/