<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

//$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
//    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
//    return $this->renderer->render($response, 'index.phtml', $args);
//});

$app->group('/usuario', function(){
	$this->post('/create', 'App\Action\UsuarioAction:create');

	$this->post('/editar', 'App\Action\UsuarioAction:editar');

	$this->get('/buscar', 'App\Action\UsuarioAction:buscar');

	$this->get('/dashboard', 'App\Action\UsuarioAction:dashboard');

	$this->get('/meus-lugares', 'App\Action\UsuarioAction:meusLugares');

	$this->get('/creditar-pontos', 'App\Action\UsuarioAction:creditarPontos');

	$this->post('/alterar-senha', 'App\Action\UsuarioAction:alterarSenha');

});

$app->group('/estabelecimento', function(){

	//cria um estabelecinmento
	$this->post('/create', 'App\Action\EstabelecimentoAction:create');

	//lista de estabalecimentos cadastrados
	$this->get('/lista-estabelecimento', 'App\Action\EstabelecimentoAction:listaEstabelecimento');

	//editar editar dados dos estabelecimento
	$this->post('/editar', 'App\Action\EstabelecimentoAction:editar');

	//buscar um estabelecimento
	$this->get('/buscar', 'App\Action\EstabelecimentoAction:buscar');

	//salva uma promocao para o estabelecimento
	$this->post('/salvar-promocao', 'App\Action\EstabelecimentoAction:salvarPromocao');

	//edita uma promocao do estabelecimento
	$this->post('/editar-promocao', 'App\Action\EstabelecimentoAction:editarPromocao');

	//buscar uma promocao do estabelecimento
	$this->get('/buscar-promocao', 'App\Action\EstabelecimentoAction:buscarPromocao');

	//listagem de promocoes do estabelecimento
	$this->get('/listar-promocoes', 'App\Action\EstabelecimentoAction:listarPromocoes');

	//exclusao de uma promocao de estabelecimento
	$this->get('/excluir-promocao', 'App\Action\EstabelecimentoAction:deletarPromocao');	

	//listagem de estabelecimentos mais proximos de acordo com a localidade do usuario
	$this->get('/lugares-proximos', 'App\Action\EstabelecimentoAction:lugaresProximos');		

	//listagem de estabelecimentos mais proximos de acordo com a localidade do usuario
	$this->get('/informacoes', 'App\Action\EstabelecimentoAction:informacoes');		

	//seguir um estabelecimento
	$this->get('/seguir', 'App\Action\EstabelecimentoAction:seguir');			

	//lista de movimentacao do usuario (creditos / resgates)
	$this->get('/lista-movimentacao', 'App\Action\EstabelecimentoAction:listaMovimentacao');			

	//historico de pontos creditados - relatorios
	$this->get('/pontos-creditados', 'App\Action\EstabelecimentoAction:pontosCreditados');	

	//resgates de promocoes - relatorios
	$this->get('/resgates-promocoes', 'App\Action\EstabelecimentoAction:resgatesPromocoes');

	//Cliente ativo e seguidores
	$this->get('/clientes', 'App\Action\EstabelecimentoAction:clientes');

	$this->get('/cliente-info', 'App\Action\EstabelecimentoAction:clienteInfo');	


});

$app->group('/auth', function(){

	//login de usuario
	$this->post('/usuario', 'App\Action\AuthAction:usuario');

	//login de estabelecimento
	$this->post('/estabelecimento', 'App\Action\AuthAction:estabelecimento');

	//login usuario/estabelecimento utilizando google ou facebook
	$this->post('/social', 'App\Action\AuthAction:social');
});

$app->group('/voucher', function(){

	$this->get('/gerar-voucher', 'App\Action\VoucherAction:gerarVoucher');

	$this->get('/lista-voucher', 'App\Action\VoucherAction:listaVoucher');	

	$this->get('/visualizar', 'App\Action\VoucherAction:visualizar');	

	$this->post('/resgatar-promocao', 'App\Action\VoucherAction:resgatarPromocao');	

});


//Rotas para pagamento com PagSeguro
$app->group('/pagamento', function(){

	$this->get('/estabelecimento/[{estabelecimento}]', 'App\Action\PagSeguroAction:verificaEstabelecimento');
	$this->get('/session', 'App\Action\PagSeguroAction:SessionId');
	$this->post('/adesao', 'App\Action\PagSeguroAction:Adesao');
	$this->get('/verifica-pagamento[/{codigo_adesao}[/{estabelecimento}]]', 'App\Action\PagSeguroAction:verificaPagamento');
	$this->post('/verifica-status-pagamento', 'App\Action\PagSeguroAction:verificaStatusPagamento');
	$this->post('/analise', 'App\Action\PagSeguroAction:analise');
	$this->post('/em-processamento', 'App\Action\PagSeguroAction:processamento');
	$this->get('/sucesso', 'App\Action\PagSeguroAction:sucesso');
	$this->get('/cancelado', 'App\Action\PagSeguroAction:cancelado');
	

});

$app->get('/notAuthorized', function ($request, $response) {

	// Render error view
	return $this->renderer->render($response, 
									'403.phtml');    

});