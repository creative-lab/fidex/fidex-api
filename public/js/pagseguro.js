(function(){

    $.ajax({
            url : $('base').attr('href') + '/pagamento/session',
            type : 'get',
            dataType : 'json',
            async : false,
            timeout: 20000,
            success: function(data){
                console.log(data.sessionid);
                $('#sessionId').val(data.sessionid);
                PagSeguroDirectPayment.setSessionId(data.sessionid);

            }
    });

    $('#cartao').blur(function(){

        //pega tipo de cartao de credito
        PagSeguroDirectPayment.getBrand({
            cardBin: $('#cartao').val(),
            success: function(response) {
                brand = response.brand.name;
                $('#brand').val(response.brand.name) ;
            },
            error: function(response) {
                document.getElementById('cartao').setCustomValidity("Cartao invalido");

                return false;
            },
            complete: function(response) {
               document.getElementById('cartao').setCustomValidity("");
            }
        });

    });

    $("#cvv").focus(function(){ //gera identificação do usuário

        var brand = null;
        //identificacao do usuario
        PagSeguroDirectPayment.onSenderHashReady(function(response){
            if(response.status == 'error') {
                console.log(response.message);
                alert(response.message);
                return false;
            }
            console.log(response.senderHash);
            $("#hashPagSeguro").val(response.senderHash);//Hash estará disponível nesta variável.
        });

    });


    $('#enviar-pagamento').click(function(){

        document.getElementById('cpf').setCustomValidity("");

        if(document.getElementById('payment').checkValidity() == false) {
            document.getElementById('payment').reportValidity();
            return false;
        }

        var valida = validaCpf();

        if(valida == false) {

            document.getElementById('cpf').setCustomValidity("Cpf invalido");
            document.getElementById('cpf').reportValidity();
            return false;

        } 

        $("#divCarregando").show();
        //gera token do cartao de credito
        PagSeguroDirectPayment.createCardToken({
                    cardNumber: $('#cartao').val(),
                    brand: $('#brand').val(),
                    cvv: $('#cvv').val(),
                    expirationMonth: $('#mes').val(),
                    expirationYear: $('#ano').val(),
                    success: function(response) {
                        $('#cardToken').val(response.card.token);

                        setTimeout(function() { $('#payment').submit(), 3000});
                                   
                    },
                    error: function(response) {

                        if(response.error){
                            console.log(response.errors);

                            if(response.errors.hasOwnProperty('30400')){
                                alert('Cartão de crédito inválido.');
                            }
                                    

                            if(response.errors.hasOwnProperty('10000')){
                                alert('Tipo de cartão inválido.');
                            }

                            if(response.errors.hasOwnProperty('10006')){
                                alert('Código de segurança inválido.');
                            }

                            if(response.errors.hasOwnProperty('30405')){
                                alert('Data de validade inválida.');
                            }

                                
                        }          

                    },
                    complete: function(response) {
                        document.getElementById('cartao').setCustomValidity("");
                        $('#divCarregando').fadeOut('slow');
                    }
                });
        
    });
    

})(jQuery) ; 

function validaCpf(){
    var campoCpf = $('#cpf').val();
    value = campoCpf.replace('.','');
    value = value.replace('.','');
    cpf = value.replace('-','');
    while(cpf.length < 11) cpf = "0"+ cpf;
        var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
        var a = [];
        var b = new Number;
        var c = 11;
        for (i=0; i<11; i++){
            a[i] = cpf.charAt(i);
            if (i < 9) b += (a[i] * --c);
        }
        if ((x = b % 11) < 2) {
            a[9] = 0
        } else {
            a[9] = 11-x
        }
        b = 0;
        c = 11;
        for (y=0; y<10; y++) b += (a[y] * c--);
        if ((x = b % 11) < 2) {
            a[10] = 0;
        } else {
            a[10] = 11-x;
        }
        if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return false;

        return true;
}


function listaMeiosPagamento() {

    var urlPagSeguro = 'https://stc.pagseguro.uol.com.br';

/*    PagSeguroDirectPayment.getPaymentMethods({
            amount: 24.90,
            success: function(response){ 
                console.log(response.paymentMethods.CREDIT_CARD)

                var paymentMethods = response.paymentMethods.CREDIT_CARD.options;

                $.each(paymentMethods , function( index, value ) {
                    if (index == 'ELO' || index == 'VISA' || index == 'MASTERCARD') {
                        $('#tipos_pagamento_credito ul')
                            .append('<li><img src="'+ urlPagSeguro + value.images.SMALL.path + '"></li>');
                    }

                });
            },
            error: function(response){ 
            }
        });
*/

}


