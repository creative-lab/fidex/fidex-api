(function(){

    var qtd = 0;

    setInterval(function() {    
            
        if(qtd < 4 ) {

            data = verifica();
            console.log(data);
            if(data.status == 5 ){//pagamento aprovado
                window.location.href = $('base').attr('href') + 'pagamento/sucesso';
            }

            if(data.status == 7 ){//pagamento cancelado
                window.location.href = $('base').attr('href') + 'pagamento/cancelado';   
            }

            qtd++;

        } else {

            $("[name=form-analise]").attr('action', $('base').attr('href') + 'pagamento/em-processamento');
            $("[name=form-analise]").submit();
            //window.location.href = $('base').attr('href') + 'pagamento/em-processamento';   
        }
                
        
    } , 10000);


})(jQuery) ; 

function verifica()
{
    var resposta = null;   
    $.ajax({
            url : $('base').attr('href') + '/pagamento/verifica-status-pagamento',
            type : 'post',
            dataType : 'json',
            data : {
                estabelecimento : $("#estabelecimento").val(),
                codigo_adesao   : $("#codigo_adesao").val()
            },
            async : false,
            //timeout: 100000,
            error: function(response){
                alert('Problemas de comunicação com a operadora do seu cartao. Tentando novamente');                
                resposta = response;
            },
            success: function(response){
                resposta =  response;                 
            }
    });

    return resposta;
}