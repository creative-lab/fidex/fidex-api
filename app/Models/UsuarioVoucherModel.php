<?php

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Capsule\Manager as DB;


/**
*
*/
class UsuarioVoucherModel extends Model
{

    protected $table = 'usuario_voucher';
    protected $fillable = ['usuario_id',
                            'promocao_id',
                            'codigo',
                            'status',
                            'pontos'
                        ];


    public function listaVoucher($usuarioId)
    {
        return $this->select(
                    'ep.promocao',
                    'uv.pontos',
                    'e.nome',
                    'uv.id as voucher_id',
                    'e.id as estabelecimento_id'
                )
                ->from('usuario_voucher as uv')
                ->join('usuario as u', 'u.id', '=', 'uv.usuario_id' )
                ->join('estabelecimento_promocao as ep', 'ep.id', '=', 'uv.promocao_id' )
                ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                ->where('uv.status', '=', 1)
                ->where('u.id', '=', $usuarioId)
                //->whereBetween('uv.created_at', ['DATE_ADD(CURRENT_DATE(),INTERVAL -30 DAY)', 'current_date()'])
                ->whereRaw('DATE(uv.created_at) between DATE_ADD(CURRENT_DATE(),INTERVAL -30 DAY) and current_date()')
                ->orderBy('uv.created_at', 'DESC')
                ->get();

    }

    public static function validaVoucher($codigo, $estabelecimento_id, $cpf)
    {

        $voucher = UsuarioVoucherModel::select('uv.codigo', 'uv.created_at', 'uv.status', 'uv.id')
                    ->from('usuario_voucher as uv')
                    ->join('estabelecimento_promocao as ep', 'ep.id', '=', 'uv.promocao_id' )
                    ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                    ->join('usuario as u', 'u.id', '=', 'uv.usuario_id' )
                    //->where('codigo', '=', $codigo)
                    ->whereRaw("BINARY `codigo` = ?", [$codigo])
                    ->where('ep.estabelecimento_id', '=', $estabelecimento_id)
                    ->where('u.cpf', '=', $cpf)
                    //->where('uv.status', '=', 1)
                    ->first();

        if(is_null($voucher)) {

            throw new \Exception("Voucher inválido");
        
        } else if($voucher->status == 2) {

            throw new \Exception("Voucher já utilizado");

        } else if($voucher->status == 3) {

            throw new \Exception("Voucher expirado");

        } 


        $data = new \DateTime();

        $hoje = $data->getTimeStamp();

        $dataVoucher = $voucher->created_at->addDays('30')->getTimeStamp();

        if($hoje > $dataVoucher) {

            $voucher->status = 3;
            $voucher->save();

            throw new \Exception("Voucher expirado");
        }

        return $voucher;

    }

     public function pontosResgatados($usuarioId, $estabelecimentoId)
    {

        return $this->select(
                    DB::raw('sum(uv.pontos) as pontos')
                )
                ->from('usuario_voucher as uv')
                ->join('estabelecimento_promocao as ep', 'ep.id', '=', 'uv.promocao_id')
                ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                ->join('usuario as u', 'u.id', '=', 'uv.usuario_id')
                ->where('u.id', '=', $usuarioId)
                ->where('e.id', '=', $estabelecimentoId)
                ->first();

    }

    public function qtdPromocoesResgatadas($estabelecimentoId, $usuarioId = NULL)
    {

        $query = $this->select(
                    DB::raw('count(uv.id) as promocoes_resgatadas')
                )
                ->from('usuario_voucher as uv')
                ->join('estabelecimento_promocao as ep', 'ep.id', '=', 'uv.promocao_id')
                ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                ->join('usuario as u', 'u.id', '=', 'uv.usuario_id')
                ->where('e.id', '=', $estabelecimentoId)
                ->where('uv.status', '=', 2);

                if(isset($usuarioId)) {
                    $query->where('u.id', '=', $usuarioId);    
                }
                
        return $query->first();

    
    }

    public function totalPontosUtilizados($estabelecimentoId, $usuarioId = NULL)
    {

        $query = $this->select(
                    DB::raw('sum(uv.pontos) as pontos')
                )
                ->from('usuario_voucher as uv')
                ->join('estabelecimento_promocao as ep', 'ep.id', '=', 'uv.promocao_id')
                ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                ->join('usuario as u', 'u.id', '=', 'uv.usuario_id')
                ->where('e.id', '=', $estabelecimentoId)
                ->where('uv.status', '=', 2);

                if(isset($usuarioId)) {
                    $query->where('u.id', '=', $usuarioId);    
                }
                
        return $query->first();

    
    }

    public function resgatesPromocoes($estabelecimentoId)
    {
        return $this->select(
                        DB::raw('DATE_FORMAT(uv.updated_at, "%d/%m/%Y") as data'),
                        DB::raw('TIME_FORMAT(uv.updated_at, "%H:%i") as hora'),
                        'uv.pontos',
                        'u.nome',
                        'u.cpf',
                        'ep.promocao'
                )
                ->from('usuario_voucher as uv')
                ->join('usuario as u', 'u.id', '=', 'uv.usuario_id')
                ->join('estabelecimento_promocao as ep', 'ep.id', '=', 'uv.promocao_id')
                ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                ->where('uv.status', '=', 2)
                ->where('e.id', '=', $estabelecimentoId)
                ->orderBy('uv.updated_at', 'DESC')
                ->get();

    }

}
