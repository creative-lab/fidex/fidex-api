<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;



class Model extends Eloquent
{

    public function pegaAvatar($id, $table)
    {
        return $this->select('avatar')
                ->from($table)
                ->where('id', '=', $id)->get()->first();
    }

/*	private $container;
	private $connectDb;
	private $connectFirebase;

	public function __construct($container){
		$this->container = $container;
	}

	public function getConnectFirebase(){
		if(! isset($this->connectFirebase)){
			$this->connectFirebase = $this->container->get('firebase');	
		} 

		return $this->connectFirebase;

	}

*/
}
