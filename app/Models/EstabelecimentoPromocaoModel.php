<?php

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
*
*/
class EstabelecimentoPromocaoModel extends Model
{

    protected $table = 'estabelecimento_promocao';
    protected $fillable = ['promocao',
                            'pontos',
                            'descricao',
                            'estabelecimento_id'
                        ];


    public function estabelecimento()
    {
        $query = $this->belongsTo('App\Models\EstabelecimentoModel');
    }

    public function editar($params)
    {
        $promocao = $this->find($params['id']);
        
        if ($promocao == null) {
            throw new \Exception("Promoção inválida");
        }

        $promocao->promocao      = $params['promocao'];
        $promocao->pontos        = $params['pontos'];
        $promocao->descricao     = $params['descricao'];
        
                
        $promocao->save();
    }

    public function promocoes($id, $qtd = NULL)
    {
        if(isset($qtd)){
            return $this->select(DB::raw('count(id) as qtd'))
                    ->where('estabelecimento_id', '=', $id)  
                    ->first();    
        }
        
    }

}
