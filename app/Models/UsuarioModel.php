<?php

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
*
*/
class UsuarioModel extends Model
{
    private $cpf;
    private $email;
    private $socialToken;
    private $senha;
    private $tipo;

    protected $table = 'usuario';
    protected $fillable = ['nome', 'email', 'cpf', 'telefone', 'senha', 'tipo', 'avatar', 'social_token',
                                'endereco', 'bairro', 'cidade', 'uf', 'cep'];

    public function estabelecimento()
    {
        return $this->hasMany('App\Models\EstabelecimentoModel', 'usuario_id');
    }

    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    public function setSocialToken($socialToken)
    {
        $this->socialToken = $socialToken;
    }

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function usuarioLogin()
    {
        $usuario =  $this->select('id')
                    ->where('cpf', '=', $this->cpf)
                    ->where('senha', '=', $this->senha)
                    ->where('tipo', '=', '1')
                    ->get();

        if (!isset($usuario[0]->id)) {
            throw new \Exception("Usuário ou senha inválido");
        }

        return $usuario;
    }

    public function estabelecimentoLogin()
    {
        $usuario =  $this->select('id')
                    ->where('email', '=', $this->email)
                    ->where('senha', '=', $this->senha)
                    ->where('tipo', '=', '2')
                    ->get();

        if (!isset($usuario[0]->id)) {
            throw new \Exception("Email ou senha inválido");
        }

        return $usuario;
    }

    public function socialTokenLogin()
    {
        $usuario =  $this->select('id')
                    ->where('email', '=', $this->email)
                    ->where('social_token', '=', $this->socialToken)
                    ->where('tipo', '=', $this->tipo)
                    ->get();

        if (!isset($usuario[0]->id)) {
            throw new \Exception("Email ou token inválido");
        }

        return $usuario;
    }

    public function editar($params)
    {
        $usuario = $this->find($params['id']);
        
        if ($usuario == null) {
            throw new \Exception("Usuário inválido");
        }

        $usuario->nome      = $params['nome'];
        $usuario->email     = $params['email'];
        $usuario->telefone  = $params['telefone'];
        

        if (isset($params['cidade']) && $params['cidade'] != '') {
            $usuario->cidade = $params['cidade'];
        }

        if (isset($params['uf']) && $params['uf'] != '') {
            $usuario->uf = $params['uf'];
        }

        if (isset($params['cpf']) && $params['cpf'] != '') {
            $usuario->cpf = $params['cpf'];
        }

        if (isset($params['avatar']) && $params['avatar'] != '') {
            $usuario->avatar = $params['avatar'];
        }
                
        $usuario->save();
    }

    public function alterarSenha($params)
    {
        $usuario = $this->find($params['id']);

        if ($usuario == null) {
            throw new \Exception("Usuário inválido");
        }

        if(!isset($params['senha']) && $params['senha'] == ""){
            throw new \Exception("Senha é obrigatório");   
        }

        if(!isset($params['confirmar_senha']) && $params['confirmar_senha'] == ""){
            throw new \Exception("Confirmar senha é obrigatório");   
        }

        if($params['senha'] != $params['confirmar_senha'])
        {
            throw new \Exception("A informação do campo Senha não está igual a informação do campo confirmar Senha.'");   
        }

        $usuario->attributes['senha'] = $params['nova_senha'];

        $usuario->save();
    }

    public function usuarioCpf($cpf)
    {
        $cpf = preg_replace('/[^0-9]/', '', (string) $cpf);

        $usuario = $this->where('cpf', '=', $cpf)->first();

        if(is_null($usuario)){
            throw new \Exception("Usuário não cadastrado");
            
        }

        return $usuario;
    }
}
