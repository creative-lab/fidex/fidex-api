<?php

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
*
*/
class EstabelecimentoModel extends Model
{

    protected $table = 'estabelecimento';
    protected $fillable = ['nome',
                            'email',
                            'cnpj',
                            'telefone',
                            'whatsapp',
                            'descricao',
                            'palavra_chave',
                            'categoria',
                            'avatar',
                            'facebook',
                            'instagram',
                            'twitter',
                            'usuario_id',
                            'periodo_teste',
                            'latitude',
                            'longitude',
                            'endereco',
                            'bairro',
                            'cidade',
                            'uf',
                            'cep'
                        ];


    public function usuario()
    {
        $query = $this->belongsTo('App\Models\UsuarioModel');
    }

    public function estabelecimentoPagamento()
    {
        return $this->hasMany('App\Models\EstabelecimentoPagamentoModel', 'estabelecimento_id');
    }

    public function estabelecimentoPromocao()
    {
        return $this->hasMany('App\Models\EstabelecimentoPromocaoModel', 'estabelecimento_id')
        ->orderBy('created_at', 'DESC');
    }

    public function editar($params)
    {
        $estabelecimento = $this->find($params['id']);
        
        if ($estabelecimento == null) {
            throw new \Exception("Estabelecimento inv?ido");
        }

        $estabelecimento->nome          = $params['nome'];
        $estabelecimento->email         = $params['email'];
        $estabelecimento->telefone      = $params['telefone'];
        $estabelecimento->palavra_chave = $params['palavra_chave'];
        $estabelecimento->categoria     = $params['categoria'];
        $estabelecimento->facebook      = $params['facebook'];
        $estabelecimento->instagram     = $params['instagram'];
        $estabelecimento->twitter       = $params['twitter'];
        $estabelecimento->whatsapp      = $params['whatsapp'];
        $estabelecimento->descricao     = $params['descricao'];
        $estabelecimento->endereco      = $params['endereco'];
        $estabelecimento->bairro        = $params['bairro'];
        $estabelecimento->cidade        = $params['cidade'];
        $estabelecimento->uf            = $params['uf'];
        $estabelecimento->cep           = $params['cep'];
                            

        if (isset($params['cnpj']) && $params['cnpj'] != '') {
            $estabelecimento->cnpj = $params['cnpj'];
        }

        if (isset($params['avatar']) && $params['avatar'] != '') {
            $estabelecimento->avatar      = $params['avatar'];
        }

        if (isset($params['latitude']) && $params['latitude'] != '') {
            $estabelecimento->latitude      = $params['latitude'];
        }
        
        if (isset($params['longitude']) && $params['longitude'] != '') {
            $estabelecimento->longitude      = $params['longitude'];
        }
                
        $estabelecimento->save();
    }

    public function checaSeExisteCnpj($cnpj)
    {
        $estabelecimento = $this->select('cnpj')
                                ->where('cnpj', '=', $cnpj)
                                ->get()
                                ->first();

        if (is_null($estabelecimento)) {
            return true;
        }

        throw new \Exception("Ja existe um estabelecimento cadastrado com este CNPJ");
        
    }

    public function lugaresProximos($busca = NULL, $lat = NULL, $long = NULL)
    {

        $filtro = FALSE;
        
        $lugaresProximos = $this->select('e.nome',
                                            'e.id',
                                            DB::raw('Concat("http://fidexapp.com.br/img/avatar/" ,e.avatar) as avatar'),
                                            'e.bairro',
                                            'e.categoria'
                                        );

        if(isset($lat) &&  isset($long) && $lat != '' && $long != ''){
            $filtro = TRUE;
            $lugaresProximos->select('e.nome',
                                            'e.id',
                                            DB::raw('Concat("http://fidexapp.com.br/img/avatar/" ,e.avatar) as avatar'),
                                            'e.bairro',
                                            'e.categoria',
                                            DB::raw('ROUND(6371 *
                                                    acos(
                                                    cos(radians('.$lat.')) *
                                                    cos(radians(e.latitude)) *
                                                    cos(radians('.$long.') - radians(e.longitude)) +
                                                    sin(radians('.$lat.')) *
                                                    sin(radians(e.latitude))
                                                )) AS distance'));
        }                               

        
        $lugaresProximos->from('estabelecimento as e')
                                ->orderBy('e.nome');
                               
        if(isset($busca) && $busca != "") {
            $lugaresProximos->where('e.nome', 'LIKE', '%' . $busca . '%');
        }  

        if($filtro) {
           $lugaresProximos->having('distance', '<=','40');  
        }

        return $lugaresProximos->get();
    }

    public function informacoes($usuarioId, $id)
    {

        return $this->select('e.nome',
                                'e.id',
                                'e.categoria',
                                'e.descricao',
                                'e.instagram',
                                'e.twitter',
                                'e.facebook',
                                'e.email',
                                'e.telefone',
                                'e.whatsapp',
                                'e.palavra_chave',
                                'e.endereco',
                                'e.cidade',
                                'e.bairro',
                                'e.uf',
                                DB::raw('Concat("http://fidexapp.com.br/img/avatar/" ,e.avatar) as avatar'),
                                'e.latitude',
                                'e.longitude',
                                DB::raw("case when uep.usuario_id = ".$usuarioId." then 'sim' else 'nao' end as segue")  
                                        )
                                ->from('estabelecimento as e')
                                ->leftJoin('usuario_estabelecimento_pontos as uep', 'uep.estabelecimento_id', '=', 'e.id')
                                ->where('e.id', '=', $id)
                                ->first();
    
    }

    public function ultimosLugares($idUsuario, $limit = NULL)
    {
        $retorno = $this->select(
                    'e.nome',
                    'e.id',
                    DB::raw('Concat("'.URL_API.'" ,e.avatar) as avatar'),
                    DB::raw('max(uep.created_at) as  data_ultima_visita')
                )
                ->from('estabelecimento as e')
                ->join('usuario_estabelecimento_pontos as uep', 'uep.estabelecimento_id', '=', 'e.id')
                ->join('usuario as u', 'u.id', '=', 'uep.usuario_id')
                ->where('u.id', '=', $idUsuario)
                ->where('uep.pontos', '>', '0')
                ->groupBy('e.nome', 'e.avatar','e.id')
                ->orderBy(DB::raw('max(uep.created_at)'), 'DESC');

        if(isset($limit)){
            $retorno->limit($limit);
        }
            
        return $retorno->get();


    }

    public function lugares($idUsuario)
    {
        $retorno = $this->select(
                    'e.nome',
                    DB::raw('Concat("'.URL_API.'" ,e.avatar) as avatar'),
                    'e.categoria',
                    DB::raw('sum(uep.pontos) as pontos'),
                    'e.id'
                )
                ->from('estabelecimento as e')
                ->join('usuario_estabelecimento_pontos as uep', 'uep.estabelecimento_id', '=', 'e.id')
                ->join('usuario as u', 'u.id', '=', 'uep.usuario_id')
                ->where('u.id', '=', $idUsuario)
                ->groupBy('e.nome', 'e.avatar', 'e.categoria', 'e.id');
           
        return $retorno->get();


    }

    public function listaMovimentacao($estabelecimentoId)
    {

        $credito = $this->select(
                        DB::raw("'credito' as tipo"),
                        //'uep.created_at',
                        DB::raw("DATE_FORMAT(uep.created_at, '%d/%m/%Y') as data"),
                        DB::raw("TIME_FORMAT(uep.created_at, '%H:%i') as hora"),
                        'u.nome',
                        'u.cpf',
                        'uep.vl_compra',
                        DB::raw("CASE WHEN uep.vl_compra is null then NULL End as 'promocao'"),
                        'uep.pontos'
                    )
                    ->from('usuario as u')
                    ->join('usuario_estabelecimento_pontos as uep', 'uep.usuario_id', '=', 'u.id')
                    ->join('estabelecimento as e', 'e.id', '=', 'uep.estabelecimento_id')
                    ->where('e.id', '=', $estabelecimentoId)
                    ->where('uep.pontos', '>', '0');
    
        return $this->select(
                    DB::raw("'resgate' as tipo"),
                    DB::raw("DATE_FORMAT(uv.created_at, '%d/%m/%Y') as data"),
                    DB::raw("TIME_FORMAT(uv.created_at, '%H:%i') as hora"),
                    'u.nome',
                    'u.cpf',
                    DB::raw("CASE WHEN ep.promocao is null then NULL End as 'promocao'"),
                    'ep.promocao',
                    'uv.pontos'
                )    
                ->from('usuario as u')
                ->join('usuario_voucher as uv', 'uv.usuario_id', '=', 'u.id')
                ->join('estabelecimento_promocao as ep', 'ep.id', '=', 'uv.promocao_id')    
                ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                ->where('e.id', '=', $estabelecimentoId)
                ->unionAll($credito)
                ->orderBy('data', 'DESC')
                ->get();


    }

    public function periodoTesteVencido(EstabelecimentoModel $estabelecimento)
    {

        $estabelecimento->periodo_teste = false;                            
                
        $estabelecimento->save();
    }

}
