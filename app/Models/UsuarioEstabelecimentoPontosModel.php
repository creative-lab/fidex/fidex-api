<?php

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
*
*/
class UsuarioEstabelecimentoPontosModel extends Model
{

    protected $table = 'usuario_estabelecimento_pontos';
    protected $fillable = ['usuario_id',
                            'estabelecimento_id',
                            'pontos',
                            'vl_compra'
                        ];


//    public function estabelecimento()
//    {
//        $query = $this->belongsTo('App\Models\EstabelecimentoModel');
//    }

    public function pontosUsuario($usuarioId, $estabelecimentoId){
    	
    	return $this->select(DB::raw('sum(pontos) as pontos'))
    				->where('usuario_id', '=', $usuarioId)
    				->where('estabelecimento_id', '=', $estabelecimentoId)
    				->first();
    }

    public static function preCreate($params)
    {
		$usuario = UsuarioModel::find($params['usuario_id']);    		

		if (!is_object($usuario) && count($usuario) == 0) {
			throw new \Exception('Usuario invalido');
			
		}

        if($usuario->tipo == '2')
        {
            throw new \Exception('Usuario invalido');
        }    

		$estabelecimento = EstabelecimentoModel::find($params['estabelecimento_id']);    		

		if (!is_object($estabelecimento) && count($estabelecimentos) == 0) {
			throw new \Exception('Estabelecimento invalido');
			
		}


    }

    public function qtdLugaresParticipo($idUsuario)
    {
        $retorno =  $this->select(DB::raw('count(distinct estabelecimento_id) as qtd'))
                    ->where('usuario_id', '=', $idUsuario)
                    ->first();

        return $retorno->qtd;
    }

    
    public  function qtdSeguidores($estabelecimentoId, $ativos = TRUE)
    {
        $retorno = $this->select(DB::raw('count(distinct usuario_id) as qtd'))
                    ->where('estabelecimento_id', '=', $estabelecimentoId);

        if($ativos) {
            $retorno->where('pontos', '>', 0);
        } else{
            $retorno->where('pontos', '=', 0);
        }

        return  $retorno->first();
                    
    }

    public function creditarPontos(UsuarioModel $usuario, $vlCompra, $estabelecimentoId)
    {
        $estabelecimento = EstabelecimentoModel::find($estabelecimentoId);

        if (!is_object($estabelecimento) && count($estabelecimento) == 0) {
            throw new \Exception('Estabelecimento invalido');
            
        }

        $pontos = floor($vlCompra / 3);

        $pontosUsuario = array(
            'usuario_id' => $usuario->id,
            'estabelecimento_id' => $estabelecimento->id,
            'pontos' => $pontos,
            'vl_compra' => str_replace(',', '.', $vlCompra)
        );

        return $this->create($pontosUsuario);
    }

    public function gastosPontosClientes($estabelecimentoId, $sumPontos = FALSE, $sumVlCompra = FALSE , $usuarioId = NULL)
    {
        if($sumPontos) {
             $query = $this->select(
                    DB::raw('sum(uep.pontos) as pontos')
                );  

        } else if($sumVlCompra) {

            $query = $this->select(
                    DB::raw('sum(uep.vl_compra) as vlCompra')
                );
           
        } else {
            $query = $this->select(
                    'uep.pontos',
                    'uep.vl_compra',
                    DB::raw("DATE_FORMAT(uep.created_at, '%d/%m/%Y') as data"),
                    DB::raw("TIME_FORMAT(uep.created_at, '%H:%i') as hora"),
                    'u.nome',
                    'u.cpf'                
                );
        }

         $query->from('usuario_estabelecimento_pontos as uep')
                ->join('usuario as u', 'u.id', '=', 'uep.usuario_id')
                ->where('estabelecimento_id', '=', $estabelecimentoId)
                ->where('uep.pontos', '>', 0);

        if(isset($usuarioId)) {
            $query->where('u.id', '=', $usuarioId);
        }

        return $query->get();
    

    }

    public function usuariosAtivosESeguidores($estabelecimentoId, $ativos = FALSE)
    {
        $query = $this->select(
                    'u.nome',
                    'u.cpf',
                    'u.id'
                )
                ->from('usuario_estabelecimento_pontos as uep')
                ->join('estabelecimento as e', 'e.id', '=', 'uep.estabelecimento_id' )
                ->join('usuario as u', 'u.id', '=', 'uep.usuario_id')
                ->where('e.id', '=', $estabelecimentoId);

        if($ativos) {
            $query->where('uep.pontos', '>', 0); // usuario ativos
        } else {
            $query->where('uep.pontos', '=', 0); // apenas seguindo o estabelecimento
        }
                
        return $query->groupBy('u.nome', 'u.cpf', 'u.id')
                    ->get();
    }

}
