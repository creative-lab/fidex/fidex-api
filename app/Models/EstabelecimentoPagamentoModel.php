<?php

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
*
*/
class EstabelecimentoPagamentoModel extends Model
{

	protected $table = 'estabelecimento_pagamento';
    protected $fillable = [
                            'estabelecimento_id',
                            'codigo_pagamento',
                            'codigo_adesao',
                            'codigo_chave',
                            'status'
                ];

    public function estabelecimento()
    {
        $query = $this->belongsTo('App\Models\EstabelecimentoModel');
    }

    public function pegaUltimoPagamento($id)
    {

    	return $this->select(DB::raw('DATE(MAX(ep.created_at)) as data'), 
                                        'ep.codigo_adesao', 
                                        'ep.estabelecimento_id',
                                        'ep.status',
                                        'ep.codigo_pagamento',
                                        'ep.codigo_chave',
                                        'ep.id')
    				->from('estabelecimento_pagamento as ep')
                    ->join('estabelecimento as e', 'e.id', '=', 'ep.estabelecimento_id')
                    ->where('e.id', '=',$id)
                    ->groupBy('ep.codigo_adesao', 'ep.estabelecimento_id', 'ep.status', 'ep.codigo_pagamento', 'ep.codigo_chave', 'ep.id')
                    ->first();

    }

}