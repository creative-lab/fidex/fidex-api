<?php

namespace App\Action;

use App\Action\Action;
use App\Models\EstabelecimentoModel;
use App\Models\UsuarioModel;
use App\Models\EstabelecimentoPromocaoModel;
use App\Models\UsuarioEstabelecimentoPontosModel;
use App\Models\UsuarioVoucherModel;
use App\Models\EstabelecimentoPagamentoModel;
use App\Action\PagSeguroAction;


use \Illuminate\Database\QueryException;

/**
*
*/
class EstabelecimentoAction extends Action
{

    protected $name = "Estabelecimento";
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function create($request, $response)
    {
        try {

            $params = $request->getParsedBody();

//            $this->validaEmail($params['email']);

            if (isset($params['cnpj']) && $params['cnpj'] != '') {

                $this->validaCnpj($params['cnpj']);
                $params['cnpj'] = preg_replace('/[^0-9]/', '', (string) $params['cnpj']);

                $estabelecimentoModel = new EstabelecimentoModel();
                $estabelecimentoModel->checaSeExisteCnpj($params['cnpj']);

            } else if(isset($params['cnpj']) && $params['cnpj'] == '') {
                $params['cnpj'] = NULL;
            }

            
            $params['telefone'] = preg_replace('/[^0-9]/', '', (string) $params['telefone']);

            $params['periodo_teste']  = true;

            if (!isset($params['usuario_id']) || $params['usuario_id'] == '') {
                throw new \Exception("Identificador do usuario deve ser passado.");
            }

            if (!isset($params['avatar']) || $params['avatar'] == '') {
                throw new \Exception("avatar_obrigatorio.");
            } else {
                $params['avatar'] = $this->base64ToImage($params['avatar']);
            }
            
            $create = EstabelecimentoModel::create($params);

            echo json_encode($create);

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $exc->getMessage()));

        }

    }

    private function checaPagamento(EstabelecimentoModel $estabelecimento) {

        $estPagamento = new EstabelecimentoPagamentoModel();

        $pagamento = $estPagamento->pegaUltimoPagamento($estabelecimento->id);

        if(is_null($pagamento->data)){
            throw new \Exception("periodo_teste_vencido");
            
        }

        if( $pagamento->status != 3 ){ //pagamento em analise pelo pagSeguro
            $this->checaStatusPagamentoPagSeguro($pagamento);
        };

        $intervalo = $this->intervalo($pagamento->data);      

        if( $intervalo >= 32 ) {
            $this->checaPagamentoPagSeguro($pagamento);    
        }


    }

    public function checaStatusPagamentoPagSeguro(EstabelecimentoPagamentoModel $pagamento)
    {

        $paymentRequest = new \PagSeguroPaymentRequest();  

        try {  

            $credentials = \PagSeguroConfig::getAccountCredentials();  

              $response = \PagSeguroTransactionSearchService::searchByCode(  
                $credentials,  
                $pagamento->codigo_pagamento  
              );  

                $status = $response->getStatus();

                switch ($status->getValue()) {
                    case '1':
                        throw new \Exception('aguardando_pagamento');
                        break;

                    case '2':
                        throw new \Exception('pagamento_em_analise');
                        break;
                    
                    case '3':
                    case '4':
                        $pagamento->status = 3;
                        $pagamento->save();
                        break;

                    case '7':
                        throw new \Exception('pagamento_cancelado');
                        break;
                }
            
                
                
        } catch (\PagSeguroServiceException $exc) {  
  
            throw new \Exception($exc->getMessage());
  

        } 

    }

    private function checaPagamentoPagSeguro(EstabelecimentoPagamentoModel $pagamento)
    {
        $pagSeguro = new PagSeguroAction($this->container);


        $retorno = $pagSeguro->curlPagSeguro($pagSeguro->getPaymentOrderEndPoint($pagamento->codigo_adesao  ));

        $retorno = json_decode($retorno);

        $ordemPagamento = (array) $retorno->paymentOrders;

        $my_key = $pagamento->codigo_chave;
        $keys = array_keys($ordemPagamento);
        $i = array_search($my_key, $keys);
        $next_key = $keys[$i+1];

        if(count($ordemPagamento[$next_key]->transactions) == 0) {
            throw new \Exception("aguardando_pagamento");
        }

        if($ordemPagamento[$next_key]->transactions[0]->status == 1) {
            throw new \Exception("aguardando_pagamento");
        }


        $params = array(
            'codigo_adesao'       => $pagamento->codigo_adesao, 
            'estabelecimento_id'  => $pagamento->estabelecimento_id, 
            'codigo_pagamento'    => $ordemPagamento[$next_key]->transactions[0]->code,
            'codigo_chave'        => $ordemPagamento[$next_key]->code,
            'status'              => $ordemPagamento[$next_key]->transactions[0]->status
        );

        $create = EstabelecimentoPagamentoModel::create($params);
    }

    private function checaPeriodoTeste(EstabelecimentoModel $estabelecimento) {

        $intervalo = $this->intervalo($estabelecimento->created_at->format('Y-m-d'));

        if($intervalo > 90 ){

            $estabelecimento->periodoTesteVencido($estabelecimento);

            throw new \Exception("periodo_teste_vencido");
            
            
        };        

    }

    public function listaEstabelecimento($request, $response)
    {
        try {

            $retorno = array('estabelecimentos'=>array());

            $id = $request->getParam('usuarioId');

            $estabelecimentos = UsuarioModel::find($id)->estabelecimento;

            $estabelecimento = $estabelecimentos[0];

            if($estabelecimento->periodo_teste == 0) {

                $this->checaPagamento($estabelecimento);

            } elseif ($estabelecimento->periodo_teste == 1) {

                $this->checaPeriodoTeste($estabelecimento);

            }

            if (is_object($estabelecimentos) && count($estabelecimentos) > 0) {

            $arrayEstabelecimento = array();

            foreach ($estabelecimentos as $key => $value) {
                $value->avatar = URL_API .$value->avatar;

                $arrayEstabelecimento['estabelecimentos'][$key]   = $value;                

                $estabelecimento = new EstabelecimentoPromocaoModel();
                $promocoes = $estabelecimento->promocoes($value->id, 'qtd');

                $arrayEstabelecimento['estabelecimentos'][$key]['promocoes']   = $promocoes->qtd;

                $usuPromocoes = new UsuarioEstabelecimentoPontosModel();
                $seguidores = $usuPromocoes->qtdSeguidores($value->id, FALSE);

                $arrayEstabelecimento['estabelecimentos'][$key]['seguidores']   = $seguidores->qtd;

                $ativos = $usuPromocoes->qtdSeguidores($value->id);    

                $arrayEstabelecimento['estabelecimentos'][$key]['clientes_ativos']   = $ativos->qtd;

            }


            $retorno = $arrayEstabelecimento;
            
        }        

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($retorno);

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $exc->getMessage()));

        }


    }

    public function buscar($request, $response)
    {
        $id = $request->getParam('id');
   
        $estabelecimento = EstabelecimentoModel::find($id);

        if (is_object($estabelecimento) && count($estabelecimento) > 0) {

            $estabelecimento->avatar = URL_API . $estabelecimento->avatar;

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($estabelecimento);

        } else {
            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withStatus(409)
                    ->withJson(array('error'=>'Estabelecimento invalido'));
        }


    }

    public function editar($request, $response)
    {
        try {

            $params = $request->getParsedBody();

            $this->validaEmail($params['email']);


            if (isset($params['cnpj']) && $params['cnpj'] != '') {
                $this->validaCnpj($params['cnpj']);
                $params['cnpj'] = preg_replace('/[^0-9]/', '', (string) $params['cnpj']);
            }

                $params['telefone'] = preg_replace('/[^0-9]/', '', (string) $params['telefone']);

            if (isset($params['avatar']) && $params['avatar'] != '') {
                $this->deletaAvatar($params['id'], 'estabelecimento');
                $params['avatar'] = $this->base64ToImage($params['avatar']);
            }
             
            $estabelecimento = new EstabelecimentoModel();
            $estabelecimento->editar($params);

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> $exc->getMessage()));

        } catch (QueryException $e) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $e->errorInfo[2]));
        }
    }

    public function salvarPromocao($request, $response)
    {
        try {

            $params = $request->getParsedBody();

            if (!isset($params['estabelecimento_id']) || $params['estabelecimento_id'] == '') {
                throw new \Exception("Identificador do estabelecimento deve ser passado.");
            }
            
            $create = EstabelecimentoPromocaoModel::create($params);

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson($create);

        } catch (QueryException $e) {

            if ($e->errorInfo[1] == 1364) {
                $erro = explode("'", $e->errorInfo[2]);
            
                $e->errorInfo[2] = ucfirst($erro[1]). ' é obrigatório.';

            }

            return $response->withStatus(409)
                   ->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array('error'=> $e->errorInfo[2]));

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                   ->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array('error'=> $exc->getMessage()));

        }
    }

    public function listarPromocoes($request, $response)
    {
        $retorno = array('promocoes'=>array());

        $id = $request->getParam('estabelecimentoId');

        $promocoes = EstabelecimentoModel::find($id)->estabelecimentoPromocao;

        if (is_object($promocoes) && count($promocoes) > 0) {
            $arrayPromocoes = array();

            foreach ($promocoes as $key => $value) {
                $arrayPromocoes['promocoes'][] = $value;
            }

            $retorno = $arrayPromocoes;
            
        }

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($retorno);
    }

    public function buscarPromocao($request, $response)
    {
        $id = $request->getParam('id');
   
        $promocao = EstabelecimentoPromocaoModel::find($id);

        if (is_object($promocao) && count($promocao) > 0) {

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($promocao);

        } else {
            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withStatus(409)
                    ->withJson(array('error'=>'Promoção inválida'));
        }


    }

    public function editarPromocao($request, $response)
    {
        try {

            $params = $request->getParsedBody();
            
            $promocao = new EstabelecimentoPromocaoModel();
            $promocao->editar($params);

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                ->withJson(array('success'=> 'Estabelecimento editado com sucesso'));            

        } catch (QueryException $e) {

            if ($e->errorInfo[1] == 1048) {
                $erro = explode("'", $e->errorInfo[2]);
            
                $e->errorInfo[2] = ucfirst($erro[1]). ' é obrigatório.';


                return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $e->errorInfo[2]));

            }


            return $response->withStatus(409)
                       ->withHeader('Content-type', 'application/json;charset=utf-8')
                       ->withJson(array('error'=> $e->getMessage()));



        } catch (\Exception $exc) {

            return $response->withStatus(409)
                   ->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array('error'=> $exc->getMessage()));

        }
    }

    public function deletarPromocao($request, $response)
    {

        $id = $request->getParam('id');

        $promocao = EstabelecimentoPromocaoModel::find($id);

        if (is_object($promocao) && count($promocao) > 0) {

            $promocao->delete();

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array('sucess'=> 'Promoção excluida com sucesso'));            

        } else {

            return $response->withStatus(409)
                   ->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array('error'=> 'Promoção inválida'));

        }

    }

    public function lugaresProximos($request, $response)
    {

        $lat   = $request->getParam('lat');
        $long  = $request->getParam('long');      
        $busca = $request->getParam('busca'); 
        

        $estabelecimentoModel = new EstabelecimentoModel();
        
        
        $lugaresProximos =  $estabelecimentoModel->lugaresProximos($busca, $lat, $long);


        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson($lugaresProximos);            

    }

    public function informacoes($request, $response)
    {
        $usuarioId           = $request->getParam('usuarioId');
        $estabelecimentoId   = $request->getParam('estabelecimentoId');

        $retorno = array();

        $estabelecimentoModel = new EstabelecimentoModel();
        $informacoes = $estabelecimentoModel->informacoes($usuarioId, $estabelecimentoId);

        $retorno['estabelecimento'] = $informacoes;

        if($informacoes->segue == 'sim'){

            $promocoes = EstabelecimentoModel::find($estabelecimentoId)->estabelecimentoPromocao;

            foreach ($promocoes as $key => $value) {
                unset($value->created_at, 
                    $value->updated_at, 
                    $value->estabelecimento_id, 
                    $value->descricao);

                $retorno['promocoes'][] = $value; 
            }

            $usuPromocoes = new UsuarioEstabelecimentoPontosModel();
            $pontosUsuario = $usuPromocoes->pontosUsuario($usuarioId, $estabelecimentoId);

            $voucher = new UsuarioVoucherModel();

            $pontosResgatados = $voucher->pontosResgatados($usuarioId, $estabelecimentoId);

            $retorno['pontos_usuario'] =  $pontosUsuario->pontos - $pontosResgatados->pontos;

            $resgates = $voucher->qtdPromocoesResgatadas($estabelecimentoId, $usuarioId);

            $retorno['promocoes_resgatadas'] = $resgates->promocoes_resgatadas;
        } 

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson($retorno);            
    }

    public function seguir($request, $response)
    {
        try {

            $params = array();

            $params['usuario_id']           = $request->getParam('usuarioId');
            $params['estabelecimento_id']   = $request->getParam('estabelecimentoId');
            $params['pontos']               = 0;
            $params['vl_compra']            = 0;

            UsuarioEstabelecimentoPontosModel::preCreate($params);    
            UsuarioEstabelecimentoPontosModel::create($params);    

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array('sucesso'));            

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                   ->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array('error'=> $exc->getMessage()));

        }
        

    }

    public function resgatesPromocoes($request, $response)
    {
        $id   = $request->getParam('id');

        $usuVoucher = new UsuarioVoucherModel();

        $resgates['pontos_utilizados'] = $usuVoucher->totalPontosUtilizados($id);

        $resgates['total_resgates'] = $usuVoucher->qtdPromocoesResgatadas($id);

        $resgates['lista'] = $usuVoucher->resgatesPromocoes($id);


        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array($resgates));        

    }

    public function pontosCreditados($request, $response)
    {

        $id = $request->getParam('id');

        $usuPromocao = new UsuarioEstabelecimentoPontosModel();

        $retorno['gastos_clientes'] =  $usuPromocao->gastosPontosClientes($id, FALSE, TRUE);
    
        $retorno['gastos_clientes'] = number_format($retorno['gastos_clientes'][0]->vlCompra, 2, ',', '.');

        $retorno['pontos_creditados'] =  $usuPromocao->gastosPontosClientes($id, TRUE, FALSE);      

        $retorno['lista'] =  $usuPromocao->gastosPontosClientes($id);

        foreach ($retorno['lista'] as $key => $value) {
            $retorno['lista'][$key]['vl_compra'] = number_format($retorno['lista'][$key]['vl_compra'], 2, ',', '.');  
        }

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array($retorno));        
    }

    public function clientes($request, $response)
    {
        $id = $request->getParam('id');        

        $estabelecimento = new UsuarioEstabelecimentoPontosModel();

        $retorno['ativos'] = $estabelecimento->usuariosAtivosESeguidores($id, TRUE);

        $retorno['seguidores'] = $estabelecimento->usuariosAtivosESeguidores($id);

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                   ->withJson(array($retorno));        

    }

    public function clienteInfo($request, $response)
    {
        try {

            $usuarioId = $request->getParam('usuarioId');
            $estabelecimentoId = $request->getParam('estabelecimentoId');


            $usuario = UsuarioModel::find($usuarioId);

            if(!is_object($usuario)) {

                return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('erro'=> 'Usuário inválido'));                
            }

            $resposta['usuario'] = array(
                'nome'      => $usuario->nome,
                'cidade'    => $usuario->cidade,
                'uf'        => $usuario->uf,
                'cpf'       => $usuario->cpf,
                'email'     => $usuario->email,
                'telefone'  => $usuario->telefone,
                'avatar'    => URL_API .$usuario->avatar
            );

            $usuPromocao = new UsuarioEstabelecimentoPontosModel();
            $voucher = new UsuarioVoucherModel();

            $resposta['pontos_estabelecimento'] = $usuPromocao->gastosPontosClientes($estabelecimentoId, TRUE, FALSE, $usuarioId);
            $resposta['gastos_estabelecimento'] = $usuPromocao->gastosPontosClientes($estabelecimentoId, FALSE, TRUE, $usuarioId);

            $resposta['pontos_gastos'] = $voucher->pontosResgatados($usuarioId, $estabelecimentoId);
            
            $resposta['qtd_resgatadas'] = $voucher->qtdPromocoesResgatadas($estabelecimentoId, $usuarioId);

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson($resposta);

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> $exc->getMessage()));

        }    
    }

    public function listaMovimentacao($request, $response)
    {
                
        $estabelecimentoId = $request->getParam('id');

        $estabelecimento = new EstabelecimentoModel();

        $movimentacao = $estabelecimento->listaMovimentacao($estabelecimentoId);

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson($movimentacao);                        
    }
}
