<?php

namespace App\Action;

use App\Action\Action;
use App\Models\UsuarioModel;
use App\Models\EstabelecimentoModel;
use App\Models\EstabelecimentoPromocaoModel;
use App\Models\UsuarioEstabelecimentoPontosModel;
use App\Models\UsuarioVoucherModel;
use \Illuminate\Database\QueryException;

/**
*
*/
class UsuarioAction extends Action
{
    protected $name = "usuario";

    public function create($request, $response)
    {
        try {

            $params = $request->getParsedBody();

            $this->validaEmail($params['email']);
            $this->validaCpf($params['cpf']);

            $params['senha'] = $this->generateHash($params['senha']);

            $params['cpf'] = preg_replace('/[^0-9]/', '', (string) $params['cpf']);
            $params['telefone'] = preg_replace('/[^0-9]/', '', (string) $params['telefone']);

            if (isset($params['avatar']) && $params['avatar'] != '') {
                $params['avatar'] = $this->base64ToImage($params['avatar']);
            }
            
            $create = UsuarioModel::create($params);

            echo json_encode($create);

        } catch (QueryException $e) {

            if ($e->errorInfo[1] == 1062) {
                $erro = explode("'", $e->errorInfo[2]);
                $erro = explode("_", $erro[3]);
            
                $e->errorInfo[2] = 'Já existe um usuário cadastrado com este '. $erro[0];

            }

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $e->errorInfo[2]));


        } catch (\Exception $exc) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $exc->getMessage()));

        }

    }

    public function buscar($request, $response)
    {
        $id = $request->getParam('id');
   
        $usuario = UsuarioModel::find($id);

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($usuario);

    }

    public function editar($request, $response)
    {
        try {

            $params = $request->getParsedBody();

            $this->validaEmail($params['email']);
            

            if (isset($params['cpf']) && $params['cpf'] != '') {
                $this->validacpf($params['cpf']);
                $params['cpf'] = preg_replace('/[^0-9]/', '', (string) $params['cpf']);
            }

            $params['telefone'] = preg_replace('/[^0-9]/', '', (string) $params['telefone']);


            if (isset($params['avatar']) && $params['avatar'] != '') {
                $this->deletaAvatar($params['id'], $this->name);
                $params['avatar'] = $this->base64ToImage($params['avatar']);
            }
             
            $usuario = new UsuarioModel();
            $usuario->editar($params);

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> $exc->getMessage()));

        } catch (QueryException $e) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $e->errorInfo[2]));
        }
    }

    public function alterarSenha($request, $response)
    {
        try {

            $params = $request->getParsedBody();

            $params['nova_senha'] = $this->generateHash($params['senha']);

            $usuario = new UsuarioModel();
            $usuario->alterarSenha($params);

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('sucesso'=> 'sucesso'));

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $exc->getMessage()));
        }
        

    }

    public function dashboard($request, $response)
    {
        $retorno = array();

        $estabelecimento = new EstabelecimentoModel();
        $usuPromocao = new UsuarioEstabelecimentoPontosModel();

        $id = $request->getParam('id');

        $usuario = UsuarioModel::find($id);

        $retorno['user']['nome'] = $usuario->nome;
        $retorno['user']['cidade'] = $usuario->cidade;
        $retorno['user']['perfil'] = URL_API.$usuario->avatar;
        $retorno['user']['uf'] = $usuario->uf;

        $retorno['lugaresVisitados'] = $usuPromocao->qtdLugaresParticipo($id);

        $retorno['resgateDePromocoes']  = 0;

        
        $ultimosLugares = $estabelecimento->ultimosLugares($id, 6);

        $retorno['ultimosLugares']      = $ultimosLugares;
        

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($retorno);


    }

    public function meusLugares($request, $response)
    {
        $id = $request->getParam('id');

        $estabelecimento = new EstabelecimentoModel();

        $retorno = array();

        $retorno['meusLugares'] = $estabelecimento->lugares($id);

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($retorno);

    }

    public function creditarPontos($request, $response)
    {
        try {
            $cpf = $request->getParam('cpf');
            $vlCompra = $request->getParam('vlCompra');
            $estabelecimentoId = $request->getParam('estabelecimentoId');

            $usuario = new UsuarioModel();
            $usuario = $usuario->usuarioCpf($cpf);


            $this->validaCpf($cpf);
            $this->verificaVlCompra($vlCompra);

            $usuPromocao = new UsuarioEstabelecimentoPontosModel();

            $usuPromocao->creditarPontos($usuario, $vlCompra, $estabelecimentoId);

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('sucesso'));

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> $exc->getMessage()));

        } 
    }

    
}
