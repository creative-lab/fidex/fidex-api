<?php

namespace App\Action;

use App\Models\EstabelecimentoModel;
use App\Models\UsuarioModel;

class Action
{

    const SALT = 'fide-salt-password';

    private $container;
    protected $name;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function generateHash($password)
    {
        $salt = substr(base64_encode(self::SALT), 0, 22);
        return crypt($password, '$2a$08$' . $salt);
    }

    public function validaEmail($email)
    {

        if (!isset($email)) {
            throw new \Exception("E-mail é obrigatório");
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("E-mail inválido");
        }
        
    }

    public function validaCpf($value)
    {

        $value = (string) $value;
        $cpf = trim($value);
        // somente números
        $cpf = preg_replace('/[^0-9]/', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // verificar a qtde de digitos
        if (strlen($cpf) != 11) {
            throw new \Exception("CPF inválido");
            return false;
        }

        // verificar por dígitos iguauis
        $regex = "/^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/";
        if (preg_match($regex, $cpf)) {
            throw new \Exception("CPF inválido");
            return false;
        }


        $tcpf = $cpf;
        $b = 0;
        $c = 11;
        for ($i = 0; $i < 11; $i++) {
            if ($i < 9) {
                $b += ($tcpf[$i] * --$c);
            }
        }

        $x = $b % 11;
        $tcpf[9] = ($x < 2) ? 0 : 11 - $x;

        $b = 0;
        $c = 11;
        for ($y = 0; $y < 10; $y++) {
            $b += ($tcpf[$y] * $c--);
        }

        $x = $b % 11;
        $tcpf[10] = ($x < 2) ? 0 : 11 - $x;


        if (($cpf[9] != $tcpf[9]) || ($cpf[10] != $tcpf[10])) {
            throw new \Exception("CPF inválido");
            return false;
        }
        return true;
    }

    public function validaCnpj($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

        // Valida tamanho
        if (strlen($cnpj) != 14) {
            throw new \Exception("CNPJ inválido");
            return false;
        }

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)) {
            throw new \Exception("CNPJ inválido");
            return false;
        }

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;

        if (($cnpj{13} == ($resto < 2 ? 0 : 11 - $resto)) == false) {
            throw new \Exception("CNPJ inválido");
            return false;
        }

        return true ;
    }

    public function base64ToImage($base64_string)
    {

        // split the string on commas
        $data = explode(',', $base64_string);

        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $ext = explode('/', $data[0]);

        if (array_key_exists(1, $ext)) {

            $ext = explode(';', $ext[1]);

        } else {

            return $base64_string;

        }
        

        $date = new \DateTime();

        $output_file  = $date->format('YmdHis') . '_avatar.' . $ext[0];

        // open the output file for writing
        $ifp = fopen($this->container->get('destino_avatar') . $output_file, 'wb');
                // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[ 1 ]));


        // clean up the file resource
        fclose($ifp);

        return $output_file;
    }

    public function deletaAvatar($id, $table)
    {
        $model = new \App\Models\Model();
        $avatar = $model->pegaAvatar($id, $table);

        if ($avatar->avatar == 'avatar.svg') {
            return true;
        }

        unlink($this->container->get('destino_avatar') . $avatar->avatar);
        
    }

    public function verificaVlCompra($vlCompra)
    {
        if($vlCompra < 3 ) {
            throw new \Exception('É preciso consumir no mínimo R$ 3,00.');
        }

    }

    public function geraCodigo($length=8)
    {
        $salt = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&";
        $len = strlen($salt);
        $pass = NULL;
        mt_srand(10000000*(double)microtime());
        for ($i = 0; $i < $length; $i++)
            {
                $pass .= $salt[mt_rand(0,$len - 1)];
        }
        return $pass;
    }

    public function intervalo($dataInicial)
    {
        $data1 = new \DateTime($dataInicial);
        $data2 = new \DateTime();

        $intervalo = $data1->diff( $data2 );
        $intervalo = (int) $intervalo->format('%a');

        return $intervalo;
    }
}
