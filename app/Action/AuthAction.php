<?php

namespace App\Action;

use App\Action\Action;
use App\Models\UsuarioModel;

/**
*
*/
class AuthAction extends Action
{

    public function usuario($request, $response)
    {
        try {
            
                $dados = $request->getParsedBody();

                $dados['cpf']   = preg_replace('/[^0-9]/', '', (string) $dados['cpf']);
                $dados['senha'] = $this->generateHash($dados['senha']);

                $usuario = new UsuarioModel();

                $usuario->setCpf($dados['cpf']);
                $usuario->setSenha($dados['senha']);

                $usuario = $usuario->usuarioLogin()->toArray();

                return $response->withStatus(200)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson($usuario[0]);


        } catch (\Exception $exc) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $exc->getMessage()));

        }
    }

    public function estabelecimento($request, $response)
    {
        try {

                $dados = $request->getParsedBody();

                $dados['senha'] = $this->generateHash($dados['senha']);

                $usuario = new UsuarioModel();

                $usuario->setemail($dados['email']);
                $usuario->setSenha($dados['senha']);

                $estabelecimento = $usuario->estabelecimentoLogin()->toArray();

                return $response->withStatus(200)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson($estabelecimento[0]);

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $exc->getMessage()));

        }

    }

    public function social($request, $response)
    {
        try {

                $dados = $request->getParsedBody();

                $usuario = new UsuarioModel();

                $usuario->setemail($dados['email']);
                $usuario->setSocialToken($dados['socialToken']);
                $usuario->setTipo($dados['tipo']);

                $usuario = $usuario->socialTokenLogin()->toArray();

                return $response->withStatus(200)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson($usuario[0]);

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                    ->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson(array('error'=> $exc->getMessage()));

        }

    }
}
