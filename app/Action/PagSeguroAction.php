<?php

namespace App\Action;

use App\Models\EstabelecimentoPagamentoModel;
use App\Models\EstabelecimentoModel;


class PagSeguroAction
{
    protected $ci;
    protected $erroCurl;
    protected $plano;

    public function __construct($ci)
    {
        $this->ci = $ci;
        $this->env =  strtoupper(getenv('PAGSEGURO_ENV')) ;

        $this->plano          = getenv('PAGSEGURO_PLANO_'.$this->env);
        $this->emailSender    = getenv('PAGSEGURO_EMAIL_SENDER_'.$this->env);
        $this->pagSeguroToken = getenv('PAGSEGURO_TOKEN_'.$this->env); 
        $this->pagSeguroEmail = getenv('PAGSEGURO_EMAIL');
        $this->url            = getenv('PAGSEGURO_URL_'.$this->env);

    }

    public function getPaymentOrderEndPoint($codAdesao)
    {
        return $this->url."pre-approvals/{$codAdesao}/payment-orders?email=".$this->pagSeguroEmail."&token=".$this->pagSeguroToken;       
    }


    public function curlPagSeguro($pagseguroApiEndPoint, $data = NULL)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $pagseguroApiEndPoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        if(isset($data)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));  
        }
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json;charset=UTF-8', 'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'));

        $retorno = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            echo "cURL Error #:" . $err; die;

        } else {
            return $retorno;
        }  
  }

  public function verificaEstabelecimento($request, $response, $args)
  {

      //verifica se existe o estabelecimento
      $estabelecimento = EstabelecimentoModel::find($args['estabelecimento']);

      if($estabelecimento === NULL) {

        // Redirect
        return $response->withRedirect($request->getUri()->getBasePath() .'/notAuthorized', 302);
      
      }

      //verifica se ja tem uma adesao valida para nao duplicar um pagamento
      $adesao = EstabelecimentoModel::find($args['estabelecimento'])->estabelecimentoPagamento()->first();

      
      if($adesao !== NULL) {
          // Redirect
          return $response->withRedirect($request->getUri()->getBasePath() .'/notAuthorized', 302);
      }
      // Render index view
      return $this->ci->renderer->render($response, 
                      'form-pagamento.phtml', 
                      [
                        'base_url' => $request->getUri()->getBasePath(), 
                        'estabelecimento' => $estabelecimento
                      ]);
  }
    

  // Return Session Id PagSeguro
  public function SessionId($req, $res)
  {
      // PagSeguro Libraries
      \PagSeguroLibrary::init();
          \PagSeguroConfig::init();
          \PagSeguroResources::init();

      //PagSeguro credentials via dot env file
      $credentials = \PagSeguroConfig::getAccountCredentials();
          $data = [
        'sessionid' => \PagSeguroSessionService::getSession($credentials),
      ];

          return $res->withHeader('Content-type', 'application/json;charset=utf-8')
                    ->withJson($data);

  }

  public function Adesao($request, $response)
  {

    $postData = $request->getParsedBody();

    $data = array(
      'plan' => $this->plano, //plano Sandbox
//'reference' => $plan_data['plan_reference'],
      'sender' => array(
          "name" => $postData["nome"],
          "email" => $this->emailSender,
//"ip" => $_SERVER['REMOTE_ADDR'],
          "hash" => $postData['hashPagSeguro'],
          "phone" => array(
              "areaCode" => $postData["ddd"],
              "number" => $postData["telefone"]
          ),
          "address" => array(
              "street" => "Av. Brigadeira Faria Lima",
              "number" => "1384",
              "complement" => "3 andar",
              "district" => "Jd. Paulistano",
              "city" => "São Paulo",
              "state" => "SP",
              "country" => "BRA",
              "postalCode" => "01452002"
          ),
          "documents" => array(array(
              "type" => "CPF",
              "value" => preg_replace('/[^0-9]/', '', (string) $postData["cpf"])
          )),
        ),
      'paymentMethod' => array(
          'type' => 'CREDITCARD',
          'creditCard' => array(
              'token' => $postData["cardToken"],
              'holder' => array(
                'name' => $postData["nome"],
                'birthDate' => $postData['data_nascimento'],
                'documents' => array(array(
                    "type" => "CPF",
                    "value" => preg_replace('/[^0-9]/', '', (string) $postData["cpf"])
                )),
                "phone" => array(
                  "areaCode" => $postData["ddd"],
                  "number" => $postData["telefone"]
                )
              ),
          ),
      ),
    );

      $pagseguroApiEndPoint = $this->url."pre-approvals?email=".$this->pagSeguroEmail."&token=".$this->pagSeguroToken; 

      $retorno = json_decode($this->curlPagSeguro($pagseguroApiEndPoint, $data));

      if(isset($retorno->error)){
        print_r($retorno); //verificar erros para tratamento.
        die;
      }
      
      // Redirect
      return $response->withRedirect($request->getUri()->getBasePath() .'/pagamento/verifica-pagamento/'.$retorno->code . '/' . $postData['estabelecimento']);
    

  }

    public function verificaPagamento($request, $response, $args)
    {

      $codAdesao = $args['codigo_adesao'];


      $retorno = $this->curlPagSeguro($this->getPaymentOrderEndPoint($codAdesao));

      $ordemPagamento = json_decode($retorno);

      foreach ($ordemPagamento->paymentOrders as $key => $value) {

        if(count($value->transactions) == 0){
            continue;
        }

        $params = array(
            'codigo_adesao'       => $codAdesao, 
            'estabelecimento_id'  => $args['estabelecimento'], 
            'codigo_pagamento'    => $value->transactions[0]->code,
            'codigo_chave'        => $value->code,
            'status'              => $value->transactions[0]->status
        );

          if($value->status == 2) { //Em analise
            // Render  view
            return $this->ci->renderer->render($response, 
                  'analise.phtml', 
                  $params + ['base_url' => $request->getUri()->getBasePath()]
                  );
          }

          if($value->status == 5) {
          

              $create = EstabelecimentoPagamentoModel::create($params);

              return $this->ci->renderer->render($response, 
                  'pagamento-finalizado.phtml');
          }
          
      }


    }

    public function analise($request, $response)
    {
        return $this->ci->renderer->render($response, 
                  'analise.phtml',
                [
                    'base_url' => $request->getUri()->getBasePath(), 
                ]);
    }

    public function sucesso($request, $response)
    {
        return $this->ci->renderer->render($response, 
                  'sucesso.phtml',
                [
                    'base_url' => $request->getUri()->getBasePath(), 
                ]);
    }

    public function cancelado($request, $response)
    {
        return $this->ci->renderer->render($response, 
                  'cancelado.phtml',
                [
                    'base_url' => $request->getUri()->getBasePath(), 
                ]);
    }


    public function processamento($request, $response)
    {

        $dados = $request->getParsedBody();


        $create = EstabelecimentoPagamentoModel::create($dados);


        return $this->ci->renderer->render($response, 
                  'em-processamento.phtml',
                [
                    'base_url' => $request->getUri()->getBasePath(), 
                ]);
    }

    public function verificaStatusPagamento($request, $response, $args)
    {
        $codAdesao = $request->getParam('codigo_adesao');

        $retorno = $this->curlPagSeguro($this->getPaymentOrderEndPoint($codAdesao));

        $ordemPagamento = json_decode($retorno);        

        foreach ($ordemPagamento->paymentOrders as $key => $value) {

            if(count($value->transactions) == 0){
                continue;
            }

            if($value->status == 5) {
              
                $params = array(
                    'codigo_adesao'       => $codAdesao, 
                    'estabelecimento_id'  => $args['estabelecimento'], 
                    'codigo_pagamento'    => $value->transactions[0]->code,
                    'codigo_chave'        => $value->code,
                    'status'              => $value->status
                );

                $create = EstabelecimentoPagamentoModel::create($params);

                return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                      ->withJson(array('status' => 5));

            } else {

                return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                      ->withJson(array('status' => $value->status));
            }


        }
    }
}
