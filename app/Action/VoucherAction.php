<?php

namespace App\Action;

use App\Action\Action;
use App\Models\UsuarioVoucherModel;
use App\Models\EstabelecimentoPromocaoModel;
use App\Models\UsuarioEstabelecimentoPontosModel;


/**
*
*/
class VoucherAction extends Action
{
	public function listaVoucher($request, $response)
    {
        $id = $request->getParam('usuarioId');
        $voucher = new UsuarioVoucherModel();

        $usuarioVoucher['wallets'] = $voucher->listaVoucher($id);

        return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson($usuarioVoucher);                        
    }

    public function gerarVoucher($request, $response)
    {
        try {
            $id         = $request->getParam('usuarioId');
            $promocaoId = $request->getParam('promocaoId');  

            $promocao = EstabelecimentoPromocaoModel::find($promocaoId);  

            $usuPontos = new UsuarioEstabelecimentoPontosModel();
            $pontos = $usuPontos->pontosUsuario($id, $promocao->estabelecimento_id);

            //verifica se usuario possui a qtd de pontos necessaria para gerar o voucher
            if($pontos->pontos < $promocao->pontos){

                return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> 'Usuário não possui pontos suficientes para essa promoção'));                            

            }

            $usuVoucher = array(
                'codigo'        => $this->geraCodigo(),
                'usuario_id'    => $id,
                'promocao_id'   => $promocaoId,
                'status'        => 1,
                'pontos'        => $promocao->pontos
            );

            UsuarioVoucherModel::create($usuVoucher);

            return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('sucesso'));                        

        } catch (\Exception $exc) {

            return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> $exc->getMessage()));            

        }

    }


    public function visualizar($request, $response)
    {
		$id 		= $request->getParam('id');      	
		$usuarioId 	= $request->getParam('usuarioId');      	

		$voucher = UsuarioVoucherModel::find($id);

		if($voucher->usuario_id != $usuarioId) {
			return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> 'Voucher invalido.'));            			
		}

		$promocao = EstabelecimentoPromocaoModel::find($voucher->promocao_id);

		$retorno['promocao'] = array(
			'promocao'  => $promocao->promocao,
			'descricao' => $promocao->descricao,
		);

		$retorno['voucher'] = array(
			'codigo' 			=> $voucher->codigo,
			'validade_inicial'  => $voucher->created_at->format('d/m/Y'),
			'validade_final' 		=> $voucher->created_at->addDays('30')->format('d/m/Y')
		);
		
		return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson($retorno);                        		

    }

    public function resgatarPromocao($request, $response)
    {
    	try  {

    		$params = $request->getParsedBody();

    		$this->validaCpf($params['cpf']);

    		$voucher = UsuarioVoucherModel::validaVoucher($params['codigo'], 
    											$params['estabelecimentoId'], 
    											$params['cpf']);


    		$voucher->status = 2;

    		$voucher->save();

    		return $response->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('sucesso' => 'Promoção resgatada com sucesso'));                        		


    	} catch (\Exception $exc) {

    		return $response->withStatus(409)
                        ->withHeader('Content-type', 'application/json;charset=utf-8')
                        ->withJson(array('error'=> $exc->getMessage()));            			

    	}
    	
    }
}